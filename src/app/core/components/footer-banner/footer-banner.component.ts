import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'vsf-footer-banner',
  templateUrl: './footer-banner.component.html',
  styleUrls: ['./footer-banner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterBannerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
