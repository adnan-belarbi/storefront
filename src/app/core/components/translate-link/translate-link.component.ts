import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: "vsf-translate-link",
    templateUrl: "./translate-link.component.html",
    styleUrls: ["./translate-link.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TranslateLinkComponent implements OnInit {
    lang: string;
    open: boolean = false;

    constructor(public translate: TranslateService) {}

    ngOnInit(): void {
        this.translate.use(localStorage.getItem("lang") || "fr");
    }

    handleLang(item: string) {
        localStorage.setItem("lang", item);
        this.translate.use(item);
        this.open = false;
        
        window.location.reload();
    }
}
