import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { filter, map, switchMap, withLatestFrom, shareReplay } from 'rxjs/operators';

import { AddToCart, GetProductDetail } from '../../../common/generated-types';
import { notNullOrUndefined } from '../../../common/utils/not-null-or-undefined';
import { DataService } from '../../providers/data/data.service';
import { NotificationService } from '../../providers/notification/notification.service';
import { StateService } from '../../providers/state/state.service';

import { ADD_TO_CART, GET_PRODUCT_DETAIL, GET_PRODUCT_RELATED } from './product-detail.graphql';

@Component({
    selector: "vsf-product-detail",
    templateUrl: "./product-detail.component.html",
    styleUrls: ["./product-detail.component.scss"],
})
export class ProductDetailComponent implements OnInit, OnDestroy {
    product: GetProductDetail.Product;
    productAssets: GetProductDetail.Assets[];
    selectedAsset: { id: string; preview: string };
    selectedVariant: GetProductDetail.Variants;
    options: string[] = [];
    qty = 1;
    breadcrumbs: GetProductDetail.Breadcrumbs[] | null = null;
    @ViewChild("addedToCartTemplate", { static: true })
    private addToCartTemplate: TemplateRef<any>;
    private sub: Subscription;

    productRelated$: Observable<any[]>;
    productRelatedLoaded$: Observable<boolean>;
    readonly placeholderProducts = Array.from({ length: 12 }).map(() => null);

    constructor(
        private dataService: DataService,
        private stateService: StateService,
        private notificationService: NotificationService,
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

    ngOnInit() {
        this.product = null!;
        const lastCollectionSlug$ = this.stateService.select(
            (state) => state.lastCollectionSlug
        );
        const productSlug$ = this.route.paramMap.pipe(
            map((paramMap) => paramMap.get("slug")),
            filter(notNullOrUndefined)
        );

        this.sub = productSlug$
            .pipe(
                switchMap((slug) => {
                    return this.dataService.query<
                        GetProductDetail.Query,
                        GetProductDetail.Variables
                    >(GET_PRODUCT_DETAIL, {
                        slug,
                    });
                }),
                map((data) => data.product),
                filter(notNullOrUndefined),
                withLatestFrom(lastCollectionSlug$)
            )
            .subscribe(([product, lastCollectionSlug]) => {
                this.product = product;
                if (this.product.assets.length > 0) {
                    this.productAssets = this.product.assets;
                }
                if (this.product.variants.length > 0) {
                    this.product.variants[0].options.map((option) => {
                        this.options.push(option.code);
                    });
                }
                if (this.product.featuredAsset) {
                    this.selectedAsset = this.product.featuredAsset;
                }
                this.selectedVariant = product.variants[0];
                const collection = this.getMostRelevantCollection(
                    product.collections,
                    lastCollectionSlug
                );
                this.breadcrumbs = collection ? collection.breadcrumbs : [];
                if (this.product) {
                    this.productRelated$ = this.dataService
                        .query(GET_PRODUCT_RELATED, {
                            id: Number(this.product.id),
                            collectionSlug: collection.slug,
                        })
                        .pipe(
                            map((data) => data.search.items)
                            //shareReplay(1)
                        );
                    this.productRelatedLoaded$ = this.productRelated$.pipe(
                        map((items) => 0 < items.length)
                    );
                }
            });
    }

    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    onChangeOption(index: number, code: string) {
        this.options[index] = code;
        this.product.variants.map((variant) => {
            let foundCount = 0;
            variant.options.map((option) => {
                if (this.options.indexOf(option.code) !== -1) {
                    foundCount++;
                }
            });
            if (foundCount === variant.options.length) {
                this.selectedVariant = variant;
                if (variant.assets.length > 0) {
                    this.productAssets = variant.assets;
                } else {
                    this.productAssets = this.product.assets;
                }
            }
        });
    }

    arrayEquals(a: string[], b: string[]) {
        return (
            Array.isArray(a) &&
            Array.isArray(b) &&
            a.length === b.length &&
            a.every((val, index) => val === b[index])
        );
    }

    addToCart(variant: GetProductDetail.Variants, qty: number) {
        this.dataService
            .mutate<AddToCart.Mutation, AddToCart.Variables>(ADD_TO_CART, {
                variantId: variant.id,
                qty,
            })
            .subscribe(({ addItemToOrder }) => {
                switch (addItemToOrder.__typename) {
                    case "Order":
                        this.stateService.setState(
                            "activeOrderId",
                            addItemToOrder ? addItemToOrder.id : null
                        );
                        if (variant) {
                            this.notificationService
                                .notify({
                                    title: "Added to cart",
                                    type: "info",
                                    duration: 3000,
                                    templateRef: this.addToCartTemplate,
                                    templateContext: {
                                        variant,
                                        quantity: qty,
                                    },
                                })
                                .subscribe();
                        }
                        break;
                    case "OrderModificationError":
                    case "OrderLimitError":
                    case "NegativeQuantityError":
                    case "InsufficientStockError":
                        this.notificationService
                            .error(addItemToOrder.message)
                            .subscribe();
                        break;
                }
            });
    }

    viewCartFromNotification(closeFn: () => void) {
        this.stateService.setState("cartDrawerOpen", true);
        closeFn();
    }

    /**
     * If there is a collection matching the `lastCollectionId`, return that. Otherwise return the collection
     * with the longest `breadcrumbs` array, which corresponds to the most specific collection.
     */
    private getMostRelevantCollection(
        collections: GetProductDetail.Collections[],
        lastCollectionSlug: string | null
    ) {
        const lastCollection = collections.find(
            (c) => c.slug === lastCollectionSlug
        );
        if (lastCollection) {
            return lastCollection;
        }
        return collections.slice().sort((a, b) => {
            if (a.breadcrumbs.length < b.breadcrumbs.length) {
                return 1;
            }
            if (a.breadcrumbs.length > b.breadcrumbs.length) {
                return -1;
            }
            return 0;
        })[0];
    }
}
